<?php

$dir = $_SERVER['DOCUMENT_ROOT']. '/provas/';
$files = array_values(array_diff(scandir($dir), array('.', '..')));

$array = ['de','a','b','c','d','e','=','do','o','da','no','as','que','em','v','na','com','brasil','para','ao','os','f','das','uma','um','se',';','dos','estao','sobre','sao','como','iii','afirmativas','brasileiro','brasileira', '', 'por', 'ii', 'i', 'ou', 'nao', 'nos', 'isto' ,'1', '2', '3', '4', '5', '6', '7', '8', '9', '—', 'cfo/qc', 'na', 'nas', 'alternativa', 'correta', 'corretas', 'correspondente', 'correspondendo', 'corresponde', 'somente', 'conhecimentos', 'conhecimento'];

foreach ($files as $index => $materia):

    $text = file_get_contents($dir.$materia);
    $text = explode(" ", slugify($text));
    $text = array_count_values($text);

    $arr = [];
    foreach($text as $key => $val):
        if(!in_array($key, $array)):
            $letter = substr($key, 0, 5);
            if(!in_array($letter, $arr)){
                $arr = array_replace($arr, [$letter => ((int)$arr[$letter] + (int)$val)]);
            } else {
                $arr = array_merge($arr, [$letter => $val]);
            }
        endif;
    endforeach;

    $textFinal = [];
    foreach ($text as $key => $val):
        $letter = substr($key, 0, 5);
        if(array_key_exists($letter, $arr)):
            $textFinal = array_replace($textFinal, [$key => $arr[$letter]]);
        endif;
    endforeach;

    arsort($textFinal, SORT_NUMERIC);
    ksort($textFinal);

    $newfile = "palavras-".$materia;
    is_file($newfile) ? unlink($newfile) : null;
    $chave = null;
    foreach($textFinal as $key => $val):
        if(!in_array($key, $array)):
            if(!in_array($key, $array) && is_numeric($val) && $val > 1):
                if($chave === $val):
                    saveWord('-' .$key, $newfile);
                else:
                    $txt = ';';
                    saveWord(';', $newfile, true);
                    $chave = $val;
                    $txt = $key . ',' . $val .',';
                    saveWord($txt, $newfile);
                endif;
            endif;
        endif;
    endforeach;
    echo 'Arquivo palavras-' . $materia . ' gerado.<br>';


endforeach;

function slugify($text) {

    // alter letters specials
    $letters = array('!'=>'', '?'=>'', '.'=>'', '('=>'','”'=>'','“'=>'', '-'=>'', '–'=>'', '*'=>'', '_'=>'', ','=>'', ')'=>'', 'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', '\n'=>'', 'conhecimento'=>'', 'conhecimentos'=>'');

    // Remove white spaces
    $text = trim(preg_replace('/\s+/', ' ', $text));

    // Normalize
    $text = strtr($text, $letters);

    // lowercase
    $text = strtolower($text);

    return $text;
}


function saveWord($message = null, $fileName = 'palavras.txt', $breakLine = false){

    $error = 'Não foi possível criar o arquivo ' . $fileName . ' com a mensagem "' . $message . '"';

    if($breakLine):
        file_put_contents($fileName, $message.PHP_EOL , FILE_APPEND | LOCK_EX)or die($error);
    else:
        file_put_contents($fileName, $message , FILE_APPEND | LOCK_EX)or die($error);
    endif;


    }